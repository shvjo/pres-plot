from matplotlib import pyplot as plt
from pres_plotter import save_pres_fig

# if you use non oop api of plt
a = range(1, 8)
b = [5, 6, 7, 7, 1, 2, 8]
plt.plot(a, b)
save_pres_fig("./my_plt_fig-foo.svg", dpi=20)
print("done")

## if you use oop api

a = range(1, 8)
b = [1, 3, 1, 7, 1, 2, 8]
fig, ax = plt.subplots()
ax.plot(a, b)
plt.tight_layout()
save_pres_fig("./my_oop_fig-foo.png", fig=fig, dpi=300)
print("done")
