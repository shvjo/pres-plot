from matplotlib import pyplot
from os import path

# version = 1.0

list_figtypes = ["_sp", "_swp", "_dvp", "_dvwp", "_dhp", "_dhwp"]
list_figsizes = [
    (13.47, 19.83),
    (13.65, 24.27),
    (6.07, 8.94),
    (6.09, 10.82),
    (9.81, 14.44),
    (18.06, 14.32),
]  # wrongly oriented need in width, height
cm_to_in = lambda val: val / 2.54
list_figsizes = [
    (cm_to_in(a[1]), cm_to_in(a[0])) for a in list_figsizes
]  # Flipts the hieght width and converts to in


def save_pres_fig(savename: str, fig=pyplot.gcf(), **kwargs):

    name_base, extension = path.splitext(savename)

    for fig_size, style_name in zip(list_figsizes, list_figtypes):
        fig.set_size_inches(fig_size)
        fig.savefig(name_base + style_name + extension, **kwargs)